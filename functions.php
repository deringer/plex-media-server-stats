<?php
/**
 * Plex Media Server interface functions
 *
 * @package    PlexMediaServer
 * @copyright  2011 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */

/**
 * Retrieve all library sections
 *
 * @access public
 * @throws Exception if unable to retrieve sections
 * @returns object
 */
function get_library_sections()
{
    $return = array();
    
    $xml = simplexml_load_file( PLEX_BASEURL . '/library/sections' );

    if ( !$xml ) {
        throw new Exception( 'Unable to retrieve library sections' );
    }
    
    foreach ( $xml->Directory as $section ) {
        $attributes = $section->attributes();
        
        $key = (int) $attributes->key;
        
        $return[$key] = (string) $attributes->title;
    }

    return $return;
}

/**
 * Retrieve library section data
 *
 * @access public
 * @param string $section Section to retrieve data for
 * @param int $key Section key to retrieve data for
 * @throws Exception if unable to retrieve section data
 * @returns object
 */
function get_library_section_data( $section, $key )
{
    $xml = simplexml_load_file( PLEX_BASEURL . '/library/sections/' . $key . '/' . $section );

    if ( !$xml ) {
        throw new Exception( 'Unable to retrieve library section data' );
    }

    return $xml;
}

/**
 * Print library section data
 *
 * @access public
 * @param object $xml Library section data to parse
 * @param string $page One of $available_pages
 * @return array
 */
function print_library_section_data( $xml, $page )
{
    $return = array();

    foreach ( $xml->Video as $video ) {
        $attributes = $video->attributes();
        $media      = $video->Media->attributes();

        $id = (int) $media->id;


        if ( $attributes->type == MEDIA_TYPE_EPISODE ) {
            $return[$id]['title']         = $attributes->grandparentTitle;
            $return[$id]['show_name']     = $attributes->title;
            $return[$id]['season_string'] =
                sprintf( 'Season %d, Episode %d', $attributes->parentIndex, $attributes->index );
        } else {
            $return[$id]['title'] = $attributes->title;
        }

        if ( trim( $attributes->summary ) !== '' ) {
            $return[$id]['summary'] = $attributes->summary;
        }

        if ( isset( $attributes->viewCount ) && $attributes->viewCount > 0 ) {
            $return[$id]['watched'] = $attributes->viewCount;
        }

        if ( $page == PAGE_RECENTLY_ADDED ) {
            $return[$id]['added'] = date( 'l jS F, Y', (int) $attributes->addedAt );
        }
    }

    return $return;
}

/**
 * Generate back link based on current URL
 *
 * @access public
 * @returns string
 */
function back_a_step()
{
    $url = trim( $_SERVER['SCRIPT_URL'] );
    $url = preg_replace( '/^\//', '', $url );
    $url = preg_replace( '/\/$/', '', $url );

    $url_parts = explode( '/', trim( $url ) );
    array_pop( $url_parts );

    return join( '/', $url_parts );
}
