<?php
/**
 * Plex Media Server interface controller
 *
 * @package    PlexMediaServer
 * @copyright  2011 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
require_once dirname( __FILE__ ) . '/config.php';
require_once INTERFACE_BASEDIR . '/functions.php';

$library_sections = get_library_sections();

$tpl->display( 'header.tpl' );

$tpl->assign( 'library_sections', get_library_sections() );
$tpl->assign( 'pages_available',  $pages_available );
$tpl->display( 'menu.tpl' );

if ( isset( $_GET['p'] ) ) {
    try {
        switch ( $_GET['p'] ) {
            case PAGE_RECENTLY_ADDED:
                $xml = get_library_section_data( LIBRARY_SECTION_RECENTLY_ADDED, $_GET['section'] );
                $data = print_library_section_data( $xml, PAGE_RECENTLY_ADDED );

                $tpl->assign( 'media', $data );
                $tpl->display( 'library_section_data.tpl' );

                break;

            case PAGE_RECENTLY_VIEWED:
                $xml = get_library_section_data( LIBRARY_SECTION_RECENTLY_VIEWED, $_GET['section'] );
                $data = print_library_section_data( $xml, PAGE_RECENTLY_VIEWED );

                $tpl->assign( 'media', $data );
                $tpl->display( 'library_section_data.tpl' );
                break;

            default:
                $tpl->assign( 'title', 'Unknown Page' );
                $tpl->assign(
                    'message',
                    'Please use the navigation menu at the top. The page you requested was not found.'
                );
                $tpl->display( 'alert_message.tpl' );
                break;
        }
    } catch ( Exception $e ) {
        print $e->getMessage();
    }
}

$tpl->display( 'footer.tpl' );
