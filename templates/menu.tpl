<div class="row">
    <ul class="tabs">
        <li{if $smarty.server.SCRIPT_URL eq '/plex/'} class="active"{/if}><a href="/plex/" title="Media Home">Home</a></li>
{foreach from=$pages_available key=id item=title}
        <li class="dropdown{if isset( $smarty.get.p ) && $smarty.get.p eq $id} active{/if}" data-dropdown="dropdown">
            <a href="#" title="{$title}" class="dropdown-toggle">{$title}</a>
            <ul class="dropdown-menu">
{foreach from=$library_sections key=key item=section}
                <li><a href="/plex/{$id}/{$key}" title="{$title} {$section}">{$section}</a></li>
{/foreach}
            </ul>
        </li>
{/foreach}
    </ul>
</div>
