{foreach from=$media item=m}
    <div class="row">
        <h3>
            {$m.title}&nbsp;
{if isset( $m.show_name )}
            <small>{$m.show_name}{if isset( $m.season_string )}, {$m.season_string}{/if}</small>
{/if}
        </h3>
{if isset( $m.summary ) }
        <p>{$m.summary}</p>
{/if}
    </div>
{/foreach}
