<div class="alert-message {if isset( $class )}{$class}{else}{$smarty.const.ALERT_MESSAGE_WARNING}}{/if}">
    <p>
        <strong>{if isset( $title )}{$title}{else}Alert{/if}</strong> {$message}
    </p>
</div>
