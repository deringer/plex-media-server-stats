<?php
/**
 * Plex Media Server interface configuration
 *
 * @package    PlexMediaServer
 * @copyright  2011 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
define( 'PLEX_BASEURL', '' );
define( 'INTERFACE_BASEURL', '' );
define( 'INTERFACE_BASEDIR', dirname( __FILE__ ) );
define( 'TEMPLATE_BASEDIR', dirname( __FILE__ ) . '/templates' );
define( 'TEMPLATE_RESOURCE_DIR', '/data/smarty' );

# Pages
define( 'PAGE_RECENTLY_ADDED', 'recently_added' );
define( 'PAGE_RECENTLY_VIEWED', 'recently_viewed' );

$pages_available = array(
    PAGE_RECENTLY_ADDED     => 'Recently Added',
    PAGE_RECENTLY_VIEWED    => 'Recently Viewed',
);

# Media types
define( 'MEDIA_TYPE_EPISODE', 'episode' );

# Library section directories
define( 'LIBRARY_SECTION_RECENTLY_ADDED', 'recentlyAdded' );
define( 'LIBRARY_SECTION_RECENTLY_VIEWED', 'recentlyViewed' );

# Alert message classes
define( 'ALERT_MESSAGE_WARNING', 'warning' );
define( 'ALERT_MESSAGE_ERROR', 'error' );
define( 'ALERT_MESSAGE_SUCCESS', 'success' );
define( 'ALERT_MESSAGE_INFO', 'info' );

require_once INTERFACE_BASEDIR . '/smarty/Smarty.class.php';

$tpl = new Smarty();
$tpl->setTemplateDir( TEMPLATE_BASEDIR );
$tpl->setCompileDir( TEMPLATE_RESOURCE_DIR . '/templates_c/plex' );
$tpl->setConfigDir( TEMPLATE_RESOURCE_DIR . '/configs/plex' );
$tpl->setCacheDir( TEMPLATE_RESOURCE_DIR . '/cache/plex' );
